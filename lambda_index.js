// This is the AWS Lambda function for take url query params 
// and pass them into the AWS Athena.
// Using SUM for the aggreation of a daily usage

"use strict";

const AthenaExpress = require("athena-express"),
	aws = require("aws-sdk");

const athenaExpressConfig = {
	aws,
	db: "gentrack",
	getStats: true
};
const athenaExpress = new AthenaExpress(athenaExpressConfig);

exports.handler = async (event, context, callback) => {

	let meter = '';
	let date = '';
	
	if (event.queryStringParameters && event.queryStringParameters.meter) {
		meter = event.queryStringParameters.meter;
	}

	if (event.queryStringParameters && event.queryStringParameters.date) {
		date = event.queryStringParameters.date;
	}

	const sqlQuery = `select SUM("0:00"+"0:30"+"1:00"+"1:30"+"2:00"+"2:30"+"3:00"+"3:30"+"4:00"+"4:30"+"5:00"+"5:30"+"6:00"+"6:30"+"7:00"+"7:30"+"8:00"+"8:30"+"9:00"+"9:30"+"10:00"+"10:30"+"11:00"+"11:30"+"12:00"+"12:30"+"13:00"+"13:30"+"14:00"+"14:30"+"15:00"+"15:30"+"16:00"+"16:30"+"17:00"+"17:30"+"18:00"+"18:30"+"19:00"+"19:30"+"20:00"+"20:30"+"21:00"+"21:30"+"22:00"+"22:30"+"23:00"+"23:30") AS total from energy_meters where meter = '${meter}' AND date = '${date}' limit 1`;

	console.log(event)
	console.log(context)
	console.log(sqlQuery)
	try {
		let results = await athenaExpress.query(sqlQuery);
		callback(null, results);
	} catch (error) {
		callback(error, null);
	}
};
