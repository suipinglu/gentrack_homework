import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
## @type: DataSource
## @args: [database = "gentrack", table_name = "energy_meters", transformation_ctx = "datasource0"]
## @return: datasource0
## @inputs: []
datasource0 = glueContext.create_dynamic_frame.from_catalog(database = "gentrack", table_name = "energy_meters", transformation_ctx = "datasource0")
## @type: ApplyMapping
## @args: [mapping = [("meter", "string", "meter", "string"), ("date", "string", "date", "string"), ("0:00", "long", "0:00", "long"), ("0:30", "long", "0:30", "long"), ("1:00", "long", "1:00", "long"), ("1:30", "long", "1:30", "long"), ("2:00", "long", "2:00", "long"), ("2:30", "long", "2:30", "long"), ("3:00", "long", "3:00", "long"), ("3:30", "long", "3:30", "long"), ("4:00", "long", "4:00", "long"), ("4:30", "long", "4:30", "long"), ("5:00", "long", "5:00", "long"), ("5:30", "long", "5:30", "long"), ("6:00", "long", "6:00", "long"), ("6:30", "long", "6:30", "long"), ("7:00", "long", "7:00", "long"), ("7:30", "long", "7:30", "long"), ("8:00", "long", "8:00", "long"), ("8:30", "long", "8:30", "long"), ("9:00", "long", "9:00", "long"), ("9:30", "long", "9:30", "long"), ("10:00", "long", "10:00", "long"), ("10:30", "long", "10:30", "long"), ("11:00", "long", "11:00", "long"), ("11:30", "long", "11:30", "long"), ("12:00", "long", "12:00", "long"), ("12:30", "long", "12:30", "long"), ("13:00", "long", "13:00", "long"), ("13:30", "long", "13:30", "long"), ("14:00", "long", "14:00", "long"), ("14:30", "long", "14:30", "long"), ("15:00", "long", "15:00", "long"), ("15:30", "long", "15:30", "long"), ("16:00", "long", "16:00", "long"), ("16:30", "long", "16:30", "long"), ("17:00", "long", "17:00", "long"), ("17:30", "long", "17:30", "long"), ("18:00", "long", "18:00", "long"), ("18:30", "long", "18:30", "long"), ("19:00", "long", "19:00", "long"), ("19:30", "long", "19:30", "long"), ("20:00", "long", "20:00", "long"), ("20:30", "long", "20:30", "long"), ("21:00", "long", "21:00", "long"), ("21:30", "long", "21:30", "long"), ("22:00", "long", "22:00", "long"), ("22:30", "long", "22:30", "long"), ("23:00", "long", "23:00", "long"), ("23:30", "long", "23:30", "long")], transformation_ctx = "applymapping1"]
## @return: applymapping1
## @inputs: [frame = datasource0]
applymapping1 = ApplyMapping.apply(frame = datasource0, mappings = [("meter", "string", "meter", "string"), ("date", "string", "date", "string"), ("0:00", "long", "0:00", "long"), ("0:30", "long", "0:30", "long"), ("1:00", "long", "1:00", "long"), ("1:30", "long", "1:30", "long"), ("2:00", "long", "2:00", "long"), ("2:30", "long", "2:30", "long"), ("3:00", "long", "3:00", "long"), ("3:30", "long", "3:30", "long"), ("4:00", "long", "4:00", "long"), ("4:30", "long", "4:30", "long"), ("5:00", "long", "5:00", "long"), ("5:30", "long", "5:30", "long"), ("6:00", "long", "6:00", "long"), ("6:30", "long", "6:30", "long"), ("7:00", "long", "7:00", "long"), ("7:30", "long", "7:30", "long"), ("8:00", "long", "8:00", "long"), ("8:30", "long", "8:30", "long"), ("9:00", "long", "9:00", "long"), ("9:30", "long", "9:30", "long"), ("10:00", "long", "10:00", "long"), ("10:30", "long", "10:30", "long"), ("11:00", "long", "11:00", "long"), ("11:30", "long", "11:30", "long"), ("12:00", "long", "12:00", "long"), ("12:30", "long", "12:30", "long"), ("13:00", "long", "13:00", "long"), ("13:30", "long", "13:30", "long"), ("14:00", "long", "14:00", "long"), ("14:30", "long", "14:30", "long"), ("15:00", "long", "15:00", "long"), ("15:30", "long", "15:30", "long"), ("16:00", "long", "16:00", "long"), ("16:30", "long", "16:30", "long"), ("17:00", "long", "17:00", "long"), ("17:30", "long", "17:30", "long"), ("18:00", "long", "18:00", "long"), ("18:30", "long", "18:30", "long"), ("19:00", "long", "19:00", "long"), ("19:30", "long", "19:30", "long"), ("20:00", "long", "20:00", "long"), ("20:30", "long", "20:30", "long"), ("21:00", "long", "21:00", "long"), ("21:30", "long", "21:30", "long"), ("22:00", "long", "22:00", "long"), ("22:30", "long", "22:30", "long"), ("23:00", "long", "23:00", "long"), ("23:30", "long", "23:30", "long")], transformation_ctx = "applymapping1")
## @type: SelectFields
## @args: [paths = ["meter", "date", "0:00", "0:30", "1:00", "1:30", "2:00", "2:30", "3:00", "3:30", "4:00", "4:30", "5:00", "5:30", "6:00", "6:30", "7:00", "7:30", "8:00", "8:30", "9:00", "9:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00", "23:30"], transformation_ctx = "selectfields2"]
## @return: selectfields2
## @inputs: [frame = applymapping1]
selectfields2 = SelectFields.apply(frame = applymapping1, paths = ["meter", "date", "0:00", "0:30", "1:00", "1:30", "2:00", "2:30", "3:00", "3:30", "4:00", "4:30", "5:00", "5:30", "6:00", "6:30", "7:00", "7:30", "8:00", "8:30", "9:00", "9:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00", "23:30"], transformation_ctx = "selectfields2")
## @type: ResolveChoice
## @args: [choice = "MATCH_CATALOG", database = "gentrack", table_name = "energy_meters", transformation_ctx = "resolvechoice3"]
## @return: resolvechoice3
## @inputs: [frame = selectfields2]
resolvechoice3 = ResolveChoice.apply(frame = selectfields2, choice = "MATCH_CATALOG", database = "gentrack", table_name = "energy_meters", transformation_ctx = "resolvechoice3")
## @type: DataSink
## @args: [database = "gentrack", table_name = "energy_meters", transformation_ctx = "datasink4"]
## @return: datasink4
## @inputs: [frame = resolvechoice3]
datasink4 = glueContext.write_dynamic_frame.from_catalog(frame = resolvechoice3, database = "gentrack", table_name = "energy_meters", transformation_ctx = "datasink4")
job.commit()