# Gentrack Homework

Gentrack Homework on AWS

## What have been done so far:

1. AWS S3 for the data landing zone

2. AWS Glue:
	- a classifier for the schema of CSV file
	- a crawlers to use the classifier above to create the table "energy_meters"
	- an ETL Spark job for reading csv file from S3 and load into "energy_meters" table
	- add a trigger for cron running( i just did a manual one instead of running every hour )

3. AWS Athena:
	- only for verifying some queries 
	- make the select statement working and prepare for the API to call
	
4. AWS Lambda:
	- Nodejs code for communicating between Athena and AWS API Gateway ( attached the code as lambda_index.js )

5. AWS API Gateway:
	- create a new "gentrack_api_gateway"
	- a new route by "Create Resource"
	- new a "GET" call for this simple API, also enable the "Query String" for both of the meter and date
	- testing

## What have NOT been done:

		- AWS Redshift, planed to move the Athena layer into here, Hive is not ideal as the API usecase
		- aws-cli automation script for spinning up all the required resources
		- more testings need to be carried out, there are some embedded tests on the Lambda and API Gate although


## What could done if there is more time:

		- aws-cli for automate the deploy, cuz there are actually lots of manual clicks accross the above solution
		- Hive(Athena) is too slow for a quick API to response, want to move Redshift or DynamoDB
		- want to put in a way that can drop the existing and processed csv files (those have been ingested)
		- more data validation checks on the Lambda side to prevent any SQL issues
		- aggreate the daily usage in the Spark side rather than in sql everytime
		- AWS Cognito for the authentication and authorisation


## Example:

	https://xxxxxx.execute-api.ap-southeast-2.amazonaws.com/test?meter=EE00011&date=2019-01-01

```
	{
	  "Items": [{ total: 365}],
	  "DataScannedInMB": 0,
	  "QueryCostInUSD": 0.00004768,
	  "EngineExecutionTimeInMillis": 578,
	  "Count": 0,
	  "QueryExecutionId": "85ea7227-d9a0-4a89-b669-f034a10a8366",
	  "S3Location": "s3://athena-express-asiastaupl-2019/85ea7227-d9a0-4a89-b669-f034a10a8366.csv"
	}
```





